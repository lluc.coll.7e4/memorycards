package com.example.memorycard

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.SystemClock
import android.widget.Chronometer
import android.widget.ImageButton
import android.widget.TextView
import androidx.lifecycle.ViewModelProvider

class GameEasy : AppCompatActivity(){
    lateinit var crono: Chronometer
    lateinit var movements: TextView
    lateinit var pause: ImageButton
    lateinit var card1: ImageButton
    lateinit var card2: ImageButton
    lateinit var card3: ImageButton
    lateinit var card4: ImageButton
    lateinit var card5: ImageButton
    lateinit var card6: ImageButton
    var buttons = arrayOfNulls<ImageButton>(6)


    private lateinit var viewModel: GameEasyViewModel


    fun jugar(i: Int, cartes: Array<Card>){
        if (!viewModel.pausat) {
            buttons[i]?.setImageResource(viewModel.cartes[i].src)
            viewModel.cartes[i].girada = true
            movements.text = ("Movements: " + viewModel.movement)
            if (!cartes[i].done) {
                if (viewModel.tir && i != viewModel.lastcard) {
                    if (getResources().getDrawable(cartes[i].src).getConstantState() ==
                        getResources().getDrawable(cartes[viewModel.lastcard].src).getConstantState()
                    ) {
                        cartes[viewModel.lastcard].done = true
                        cartes[i].done = true
                        viewModel.tir = false
                        viewModel.numCardsDone++
                    } else {
                        viewModel.tir = false
                        var a = viewModel.lastcard
                        viewModel.handler.postDelayed(Runnable {
                            buttons[a]?.setImageResource(R.drawable.sobrebronze)
                            buttons[i]?.setImageResource(R.drawable.sobrebronze)
                            cartes[viewModel.lastcard].girada = false
                            cartes[i].girada = false
                        }, 500)
                    }
                } else {
                    viewModel.lastcard = i
                    viewModel.tir = true
                    viewModel.movement++
                }
                if (viewModel.numCardsDone == 3) {
                    val i = Intent(this, EndGame::class.java)
                    i.putExtra("time", SystemClock.elapsedRealtime() - crono.base)
                    i.putExtra("movements", viewModel.movement)
                    i.putExtra("game", 1500)
                    startActivity(i)
                }
            }
        }
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.easy_game)

        viewModel = ViewModelProvider(this).get(GameEasyViewModel::class.java)



        crono = findViewById(R.id.contador)
        movements = findViewById(R.id.easymovements)
        pause = findViewById(R.id.pause)
        card1 = findViewById(R.id.easycard1)
        card2 = findViewById(R.id.easycard2)
        card3 = findViewById(R.id.easycard3)
        card4 = findViewById(R.id.easycard4)
        card5 = findViewById(R.id.easycard5)
        card6 = findViewById(R.id.easycard6)


        buttons = arrayOf(card1, card2, card3, card4, card5, card6)


        movements.text = ("Movements: " + viewModel.movement)
        crono.base = viewModel.cronom
        viewModel.cronom =  crono.base
        crono.start()
        updateIU()




        pause.setOnClickListener {
            if(!viewModel.pausat){
                pause.setImageResource(R.drawable.play)
                crono.stop()
                viewModel.pauseval = SystemClock.elapsedRealtime() - crono.base
                viewModel.pausat = true
            }
            else if(viewModel.pausat){
                pause.setImageResource(R.drawable.pause)
                crono.base = SystemClock.elapsedRealtime() - viewModel.pauseval
                crono.start()
                viewModel.pausat = false
            }
        }


        card1.setOnClickListener {
            jugar(0, viewModel.cartes)
        }

        card2.setOnClickListener {
            jugar(1, viewModel.cartes)
        }

        card3.setOnClickListener {
            jugar(2, viewModel.cartes)
        }

        card4.setOnClickListener {
            jugar(3, viewModel.cartes)
        }

        card5.setOnClickListener {
            jugar(4, viewModel.cartes)
        }

        card6.setOnClickListener {
            jugar(5, viewModel.cartes)
        }
    }

    private fun updateIU(){
        for(i in 0..5){
            buttons[i]?.setImageResource(viewModel.estatCarta(i))
        }
    }
}
