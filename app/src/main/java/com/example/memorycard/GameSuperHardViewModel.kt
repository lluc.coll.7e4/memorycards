package com.example.memorycard

import android.os.Handler
import android.os.SystemClock
import androidx.lifecycle.ViewModel

class GameSuperHardViewModel: ViewModel() {
    val images = arrayOf(R.drawable.cristianototy, R.drawable.cristianototy, R.drawable.cristianototy,
        R.drawable.mbappetoty, R.drawable.mbappetoty, R.drawable.mbappetoty,
        R.drawable.messitoty, R.drawable.messitoty, R.drawable.messitoty,
        R.drawable.terstegentoty, R.drawable.terstegentoty, R.drawable.terstegentoty)
    var cartes = arrayOf<Card>()
    var tir = 0
    var movement = 0
    var firstcard = 0
    var secondcard = 0
    var numCardsDone = 0
    val handler = Handler()
    var pausat = false
    var pauseval = 0L
    var cronom = SystemClock.elapsedRealtime()



    init {
        setDataModel()
    }

    private fun setDataModel() {
        images.shuffle()
        cartes = arrayOf(Card(0, images[0]), Card(1, images[1]), Card(2, images[2]),
            Card(3, images[3]), Card(4, images[4]), Card(5, images[5]),
            Card(6, images[6]), Card(7, images[7]), Card(8, images[8]),
            Card(9, images[9]), Card(10, images[10]), Card(11, images[11]))
    }

    fun estatCarta(idCarta: Int): Int {
        if(cartes[idCarta].girada) return cartes[idCarta].src
        else return R.drawable.bluecard
    }
}