package com.example.memorycard

import android.os.Handler
import android.os.SystemClock
import androidx.lifecycle.ViewModel

class TimerGameModelView: ViewModel() {
    val images = arrayOf(R.drawable.cristianototy, R.drawable.cristianototy,
        R.drawable.mbappetoty, R.drawable.mbappetoty,
        R.drawable.messitoty, R.drawable.messitoty,
        R.drawable.terstegentoty, R.drawable.terstegentoty)
    var cartes = arrayOf<Card>()
    var tir = false
    var movement = 0
    var lastcard = 0
    var numCardsDone = 0
    val handler = Handler()
    var pausat = false
    var pauseval = 0L
    var cronom = SystemClock.elapsedRealtime()



    init {
        setDataModel()
    }

    private fun setDataModel() {
        images.shuffle()
        cartes = arrayOf(Card(0, images[0]), Card(1, images[1]), Card(2, images[2]),
            Card(3, images[3]), Card(4, images[4]), Card(5, images[5]), Card(6, images[6]), Card(7, images[7]))
    }

    fun estatCarta(idCarta: Int): Int {
        if(cartes[idCarta].girada) return cartes[idCarta].src
        else return R.drawable.bluecard
    }

    fun restart(){
        tir = false
        images.shuffle()
        for(i in 0..7){
            cartes[i].done = false
            cartes[i].girada = false
            cartes[i].src = images[i]
        }
    }
}