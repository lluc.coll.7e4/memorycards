package com.example.memorycard

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.Spinner
import com.google.android.material.dialog.MaterialAlertDialogBuilder

class Menu : AppCompatActivity() {
    lateinit var playButton: Button
    lateinit var difficuty: Spinner
    lateinit var helpbutton: Button


    override fun onCreate(savedInstanceState: Bundle?) {
        setTheme(R.style.SplashTheme)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.menu)

        playButton = findViewById(R.id.playButton)
        difficuty = findViewById(R.id.spinner)
        helpbutton = findViewById(R.id.help)

        playButton.setOnClickListener{
            if (difficuty.selectedItem.toString().equals("Easy")){
                startActivity(Intent(this, GameEasy::class.java))
            }
            else if (difficuty.selectedItem.toString().equals("Hard")){
                startActivity(Intent(this, GameHard::class.java))
            }
            else if (difficuty.selectedItem.toString().equals("Impossible")){
                startActivity(Intent(this, GameSuperHard::class.java))
            }
            else if (difficuty.selectedItem.toString().equals("Timer Mode")){
                startActivity(Intent(this, TimerGame::class.java))
            }

        }

        helpbutton.setOnClickListener {
            MaterialAlertDialogBuilder(this)
                .setTitle("Help")
                .setMessage(resources.getString(R.string.help))
                .setNegativeButton("Accept") { dialog, which ->
                }

                .show()
        }
    }
}