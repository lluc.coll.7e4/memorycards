package com.example.memorycard

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.SystemClock
import android.widget.Chronometer
import android.widget.ImageButton
import android.widget.TextView
import androidx.lifecycle.ViewModelProvider

class GameHard : AppCompatActivity(){
    lateinit var crono: Chronometer
    lateinit var movements: TextView
    lateinit var pause: ImageButton
    lateinit var card1: ImageButton
    lateinit var card2: ImageButton
    lateinit var card3: ImageButton
    lateinit var card4: ImageButton
    lateinit var card5: ImageButton
    lateinit var card6: ImageButton
    lateinit var card7: ImageButton
    lateinit var card8: ImageButton
    var buttons = arrayOfNulls<ImageButton>(8)


    private lateinit var viewModel: GameHardViewModel


    fun jugar(i: Int, cartes: Array<Card>){
        if (!viewModel.pausat) {
            buttons[i]?.setImageResource(viewModel.cartes[i].src)
            viewModel.cartes[i].girada = true
            movements.text = ("Movements: " + viewModel.movement)
            if (!cartes[i].done) {
                if (viewModel.tir && i != viewModel.lastcard) {
                    if (getResources().getDrawable(cartes[i].src).getConstantState() ==
                        getResources().getDrawable(cartes[viewModel.lastcard].src).getConstantState()
                    ) {
                        cartes[viewModel.lastcard].done = true
                        cartes[i].done = true
                        viewModel.tir = false
                        viewModel.numCardsDone++
                    } else {
                        viewModel.tir = false
                        var a = viewModel.lastcard
                        viewModel.handler.postDelayed(Runnable {
                            buttons[a]?.setImageResource(R.drawable.sobrebronze)
                            buttons[i]?.setImageResource(R.drawable.sobrebronze)
                            cartes[viewModel.lastcard].girada = false
                            cartes[i].girada = false
                        }, 500)
                    }
                } else {
                    viewModel.lastcard = i
                    viewModel.tir = true
                    viewModel.movement++
                }
                if (viewModel.numCardsDone == 4) {
                    val i = Intent(this, EndGame::class.java)
                    i.putExtra("time", SystemClock.elapsedRealtime() - crono.base)
                    i.putExtra("movements", viewModel.movement)
                    i.putExtra("game", 3000)
                    startActivity(i)
                }
            }
        }
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.hard_game)

        viewModel = ViewModelProvider(this).get(GameHardViewModel::class.java)



        crono = findViewById(R.id.contador)
        movements = findViewById(R.id.movements)
        pause = findViewById(R.id.pause)
        card1 = findViewById(R.id.card1)
        card2 = findViewById(R.id.card2)
        card3 = findViewById(R.id.card3)
        card4 = findViewById(R.id.card4)
        card5 = findViewById(R.id.card5)
        card6 = findViewById(R.id.card6)
        card7 = findViewById(R.id.card7)
        card8 = findViewById(R.id.card8)


        buttons = arrayOf(card1, card2, card3, card4, card5, card6, card7, card8)


        movements.text = ("Movements: " + viewModel.movement)
        crono.base = viewModel.cronom
        viewModel.cronom =  crono.base
        crono.start()
        updateIU()




        pause.setOnClickListener {
            if(!viewModel.pausat){
                pause.setImageResource(R.drawable.play)
                crono.stop()
                viewModel.pauseval = SystemClock.elapsedRealtime() - crono.base
                viewModel.pausat = true
            }
            else if(viewModel.pausat){
                pause.setImageResource(R.drawable.pause)
                crono.base = SystemClock.elapsedRealtime() - viewModel.pauseval
                crono.start()
                viewModel.pausat = false
            }
        }


        card1.setOnClickListener {
            jugar(0, viewModel.cartes)
        }

        card2.setOnClickListener {
            jugar(1, viewModel.cartes)
        }

        card3.setOnClickListener {
            jugar(2, viewModel.cartes)
        }

        card4.setOnClickListener {
            jugar(3, viewModel.cartes)
        }

        card5.setOnClickListener {
            jugar(4, viewModel.cartes)
        }

        card6.setOnClickListener {
            jugar(5, viewModel.cartes)
        }

        card7.setOnClickListener {
            jugar(6, viewModel.cartes)
        }

        card8.setOnClickListener {
            jugar(7, viewModel.cartes)
        }
    }

    private fun updateIU(){
        for(i in 0..7){
            buttons[i]?.setImageResource(viewModel.estatCarta(i))
        }
    }
}
