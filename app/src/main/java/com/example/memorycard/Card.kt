package com.example.memorycard

import android.view.View
import android.widget.ImageButton

data class Card (val id: Int, var src: Int, var done: Boolean = false, var girada: Boolean = false)