package com.example.memorycard

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.SystemClock
import android.widget.Chronometer
import android.widget.ImageButton
import android.widget.TextView
import androidx.lifecycle.ViewModelProvider
import com.google.android.material.dialog.MaterialAlertDialogBuilder

class GameSuperHard : AppCompatActivity(){
    lateinit var crono: Chronometer
    lateinit var movements: TextView
    lateinit var pause: ImageButton
    lateinit var card1: ImageButton
    lateinit var card2: ImageButton
    lateinit var card3: ImageButton
    lateinit var card4: ImageButton
    lateinit var card5: ImageButton
    lateinit var card6: ImageButton
    lateinit var card7: ImageButton
    lateinit var card8: ImageButton
    lateinit var card9: ImageButton
    lateinit var card10: ImageButton
    lateinit var card11: ImageButton
    lateinit var card12: ImageButton
    var buttons = arrayOfNulls<ImageButton>(12)

    private lateinit var viewModel: GameSuperHardViewModel



    fun jugar(i: Int, cartes: Array<Card>){
        if (!viewModel.pausat) {
            buttons[i]?.setImageResource(cartes[i].src)
            movements.text = ("Movements: " + viewModel.movement)
            if (!cartes[i].done) {
                if (viewModel.tir == 0) {
                    viewModel.firstcard = i
                    viewModel.tir = 1
                    viewModel.movement++
                }
                else if (viewModel.tir == 1 && viewModel.firstcard != i) {
                    if (getResources().getDrawable(cartes[i].src).getConstantState() ==
                        getResources().getDrawable(cartes[viewModel.firstcard].src).getConstantState()
                    ) {
                        viewModel.tir = 2
                        viewModel.secondcard = i
                    } else {
                        viewModel.tir = 0
                        val first = viewModel.firstcard
                        viewModel.handler.postDelayed(Runnable {
                            buttons[first]?.setImageResource(R.drawable.bluecard)
                            buttons[i]?.setImageResource(R.drawable.bluecard)
                            cartes[viewModel.firstcard].girada = false
                            cartes[i].girada = false
                        }, 500)
                    }
                }
                else if (viewModel.tir == 2 && i != viewModel.secondcard && viewModel.secondcard != viewModel.firstcard) {
                    if (getResources().getDrawable(cartes[i].src).getConstantState() ==
                        getResources().getDrawable(cartes[viewModel.firstcard].src).getConstantState()
                    ) {
                        cartes[viewModel.firstcard].done = true
                        cartes[viewModel.secondcard].done = true
                        cartes[i].done = true
                        viewModel.tir = 0
                        viewModel.numCardsDone++
                    } else {
                        viewModel.tir = 0
                        val first = viewModel.firstcard
                        val second = viewModel.secondcard
                        viewModel.handler.postDelayed(Runnable {
                            buttons[first]?.setImageResource(R.drawable.bluecard)
                            buttons[second]?.setImageResource(R.drawable.bluecard)
                            buttons[i]?.setImageResource(R.drawable.bluecard)
                            cartes[viewModel.firstcard].girada = false
                            cartes[viewModel.secondcard].girada = false
                            cartes[i].girada = false
                        }, 500)
                    }
                }
                if (viewModel.numCardsDone == 4) {
                    val i = Intent(this, EndGame::class.java)
                    i.putExtra("time", SystemClock.elapsedRealtime() - crono.base)
                    i.putExtra("movements", viewModel.movement)
                    i.putExtra("game", 5000)
                    startActivity(i)
                }
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.super_hard_game)

        viewModel = ViewModelProvider(this).get(GameSuperHardViewModel::class.java)


        crono = findViewById(R.id.contador)
        movements = findViewById(R.id.movements)
        pause = findViewById(R.id.pause)
        card1 = findViewById(R.id.card1)
        card2 = findViewById(R.id.card2)
        card3 = findViewById(R.id.card3)
        card4 = findViewById(R.id.card4)
        card5 = findViewById(R.id.card5)
        card6 = findViewById(R.id.card6)
        card7 = findViewById(R.id.card7)
        card8 = findViewById(R.id.card8)
        card9 = findViewById(R.id.card9)
        card10 = findViewById(R.id.card10)
        card11 = findViewById(R.id.card11)
        card12 = findViewById(R.id.card12)

        buttons = arrayOf(card1, card2, card3, card4, card5, card6, card7, card8, card9, card10, card11, card12)


        movements.text = ("Movements: " + viewModel.movement)
        crono.base = viewModel.cronom
        viewModel.cronom =  crono.base
        crono.start()
        updateIU()



        pause.setOnClickListener {
            if(!viewModel.pausat){
                pause.setImageResource(R.drawable.play)
                crono.stop()
                viewModel.pauseval = SystemClock.elapsedRealtime() - crono.base
                viewModel.pausat = true
                MaterialAlertDialogBuilder(this)
                    .setTitle("Help")
                    .setMessage(resources.getString(R.string.superhardhelp))
                    .setNegativeButton("Resume") { dialog, which ->
                        pause.setImageResource(R.drawable.pause)
                        crono.base = SystemClock.elapsedRealtime() - viewModel.pauseval
                        crono.start()
                        viewModel.pausat = false
                    }

                    .show()
            }
        }


        card1.setOnClickListener {
            jugar(0, viewModel.cartes)
        }

        card2.setOnClickListener {
            jugar(1, viewModel.cartes)
        }

        card3.setOnClickListener {
            jugar(2, viewModel.cartes)
        }

        card4.setOnClickListener {
            jugar(3, viewModel.cartes)
        }

        card5.setOnClickListener {
            jugar(4, viewModel.cartes)
        }

        card6.setOnClickListener {
            jugar(5, viewModel.cartes)
        }

        card7.setOnClickListener {
            jugar(6, viewModel.cartes)
        }

        card8.setOnClickListener {
            jugar(7, viewModel.cartes)
        }

        card9.setOnClickListener {
            jugar(8, viewModel.cartes)
        }

        card10.setOnClickListener {
            jugar(9, viewModel.cartes)
        }

        card11.setOnClickListener {
            jugar(10, viewModel.cartes)
        }

        card12.setOnClickListener {
            jugar(11, viewModel.cartes)
        }
    }

    private fun updateIU(){
        for(i in 0..5){
            buttons[i]?.setImageResource(viewModel.estatCarta(i))
        }
    }
}