package com.example.memorycard

import android.content.Intent
import android.os.Handler
import android.os.SystemClock
import android.widget.ImageButton
import androidx.lifecycle.ViewModel

class GameEasyViewModel: ViewModel() {
    val images = arrayOf(R.drawable.doyle, R.drawable.doyle, R.drawable.bennett, R.drawable.bennett, R.drawable.kowalski, R.drawable.kowalski)
    var cartes = arrayOf<Card>()
    var tir = false
    var movement = 0
    var lastcard = 0
    var numCardsDone = 0
    val handler = Handler()
    var pausat = false
    var pauseval = 0L
    var cronom = SystemClock.elapsedRealtime()



    init {
        setDataModel()
    }

    private fun setDataModel() {
        images.shuffle()
        cartes = arrayOf(Card(0, images[0]), Card(1, images[1]), Card(2, images[2]),
            Card(3, images[3]), Card(4, images[4]), Card(5, images[5]))
    }

    fun estatCarta(idCarta: Int): Int {
        if(cartes[idCarta].girada) return cartes[idCarta].src
        else return R.drawable.sobrebronze
    }
}