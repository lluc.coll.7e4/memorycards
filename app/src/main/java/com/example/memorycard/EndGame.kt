package com.example.memorycard

import android.content.Intent
import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider

class EndGame : AppCompatActivity() {
    lateinit var time: TextView
    lateinit var movements: TextView
    lateinit var points: TextView
    lateinit var play: Button
    lateinit var menu: Button
    lateinit var share: Button

    private lateinit var viewModel: EndGameViewModel


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.end_game)

        viewModel = ViewModelProvider(this).get(EndGameViewModel::class.java)


        viewModel.temps = (intent.getLongExtra("time", 0))/1000
        viewModel.movement = intent.getIntExtra("movements", 0)
        viewModel.numCards = intent.getIntExtra("numcards", 0)
        viewModel.game = intent.getIntExtra("game", 0)

        if(viewModel.numCards == 0) {
            viewModel.punts = viewModel.game - (viewModel.movement * 50) - (viewModel.temps * 75)
        }
        else{
            viewModel.punts = viewModel.numCards * 100L
        }

        time = findViewById(R.id.time)
        movements = findViewById(R.id.movements)
        points = findViewById(R.id.points)
        play = findViewById(R.id.playButton)
        menu = findViewById(R.id.menuButton)
        share = findViewById(R.id.share)


        time.text = ("Time: "+viewModel.temps+" s")
        if(viewModel.movement != 0) {
            movements.text = ("Movements: " + viewModel.movement)
        }
        else{
            movements.text = ("Pairs: " + viewModel.numCards)
        }
        if (viewModel.punts > 0) {
            points.text = ("" + viewModel.punts)
        }
        else{
            points.text = ("0")
        }

        play.setOnClickListener{
            when(viewModel.game){
                0->startActivity(Intent(this, Menu::class.java))
                1500->startActivity(Intent(this, GameEasy::class.java))
                3000->startActivity(Intent(this, GameHard::class.java))
                3500->startActivity(Intent(this, TimerGame::class.java))
                5000->startActivity(Intent(this, GameSuperHard::class.java))
            }
        }

        menu.setOnClickListener {
            startActivity(Intent(this, Menu::class.java))
        }

        share.setOnClickListener{
            val sendIntent = Intent()
            sendIntent.action = Intent.ACTION_SEND
            when(viewModel.game){
                1500->sendIntent.putExtra(Intent.EXTRA_TEXT, "I scored ".plus(viewModel.punts).plus(" in EASY FUT Memory Card Game"))
                3000->sendIntent.putExtra(Intent.EXTRA_TEXT, "I scored ".plus(viewModel.punts).plus(" in HARD FUT Memory Card Game"))
                3500->sendIntent.putExtra(Intent.EXTRA_TEXT, "I scored ".plus(viewModel.punts).plus(" in TIMER MODE FUT Memory Card Game"))
                5000->sendIntent.putExtra(Intent.EXTRA_TEXT, "I scored ".plus(viewModel.punts).plus(" in IMPOSSIBLE FUT Memory Card Game"))
            }
            sendIntent.type = "text/plain"
            startActivity(sendIntent)
        }
    }
}